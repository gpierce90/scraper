

### What is scraper? ###

scraper will scrape a webpage for content, and alert the user if the content exists or changes

### Can I configure what it scrapes? ###
The url to scrape, and the selector to look for is in the settings.json file.

### How does it alert ###
Currently, it alerts to a single user via the Firebase Cloud Messaging.
The messaging implementation can be changed in the messenger.js file. 

### Files of interest ###
* app.js - This is the access point of the application. It runs the scraper on the timer.
* src/scraper.js - This makes the request to the configured url, and searches for the elements that match the configured selector.
* src/messenger.js - This sends a message when the elements are different from the last scrape
* app folder - This is an example app that I hosted to grab a requestToken for the push notifications
