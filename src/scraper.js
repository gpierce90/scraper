var cheerio = require('cheerio')
var rp = require('request-promise')
var settings = require('../config/settings.json')
var _ = require('lodash')
var messenger = require('./messenger')
var elements
module.exports = {
  findNewElements: findNewElements
}
function findNewElements () {
  var newElements = []
  rp(settings.url)
    .then(function (htmlString) {
      var $ = cheerio.load(htmlString)

      $(settings.selector).each(function () {
        newElements.push($(this).text())
      })
      if (!_.isEqual(elements, newElements)) {
        console.log('found diff')
        console.log(newElements)
        messenger.push(newElements)
        elements = newElements
      } else {
        console.log('found same')
      }
    })
    .catch(function (err) {
      console.log(err)
    })
}
