var admin = require('firebase-admin')
var serviceAccount = require('../config/scraper-ef04d-firebase-adminsdk-ut4cg-996fd89180.json')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://scraper-ef04d.firebaseio.com'
})
function push (elements) {
// This registration token comes from the client FCM SDKs.
  var registrationToken = 'bk3RNwTe3H0:CI2k_HHwgIpoDKCIZvvDMExUdFQ3P1...'

// See the "Defining the message payload" section below for details
// on how to define a message payload.
  var payload = {
    data: {
      score: '850',
      time: '2:45'
    }
  }

// Send a message to the device corresponding to the provided
// registration token.
  admin.messaging().sendToDevice(registrationToken, payload)
  .then(function (response) {
    // See the MessagingDevicesResponse reference documentation for
    // the contents of response.
    console.log('Successfully sent message:', response)
  })
  .catch(function (error) {
    console.log('Error sending message:', error)
  })
}
module.exports = {
  push: push
}
